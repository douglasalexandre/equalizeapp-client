package com.douglasalexandre;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by bluesprogrammer on 15/06/14.
 */
public class ListAlunoAdapter extends BaseAdapter{

    private Activity context;
    private ArrayList<Map<String, String>> listItems;

    static class ViewHolder{
        public TextView nomeAlunoTextView;
        public TextView cursoAlunoTextView;

    }

    public ListAlunoAdapter(Activity context, ArrayList<Map<String, String>>listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View listItemView = view;

        if (view == null){

            LayoutInflater inflater = context.getLayoutInflater();
            listItemView = inflater.inflate(R.layout.list_item_aluno, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.nomeAlunoTextView = (TextView) listItemView.findViewById(R.id.nomeAlunoTextView);
            viewHolder.cursoAlunoTextView = (TextView) listItemView.findViewById(R.id.cursoAlunoTextView);
            listItemView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) listItemView.getTag();
        Map<String, String> parada = (Map<String, String>) getItem(position);
        holder.nomeAlunoTextView.setText(parada.get("nome"));
        holder.cursoAlunoTextView.setText(parada.get("curso"));

        return listItemView;
    }
}
