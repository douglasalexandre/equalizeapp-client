package com.douglasalexandre;

import android.app.ListActivity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.douglasalexandre.alunoequalizeendpoint.Alunoequalizeendpoint;
import com.douglasalexandre.alunoequalizeendpoint.model.AlunoEqualize;
import com.douglasalexandre.alunoequalizeendpoint.model.CollectionResponseAlunoEqualize;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.json.gson.GsonFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bluesprogrammer on 15/06/14.
 */
public class ListAlunoFragment extends ListFragment {

    private ListAlunoAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        class AlunoListAsyncTask extends AsyncTask<Void, Void, CollectionResponseAlunoEqualize> {
            Context context;
            private ProgressDialog pd;

            public AlunoListAsyncTask(Context context) {
                this.context = context;
            }

            protected void onPreExecute(){
                super.onPreExecute();
                pd = new ProgressDialog(context);
                pd.setMessage("Carregando...");
                pd.show();
            }

            protected CollectionResponseAlunoEqualize doInBackground(Void... unused) {
                CollectionResponseAlunoEqualize alunos = null;
                try {
                    Alunoequalizeendpoint.Builder builder = new Alunoequalizeendpoint.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), null);
                    Alunoequalizeendpoint service =  builder.build();
                    alunos = service.listAlunoEqualize().execute();
                } catch (Exception e) {
                    Log.d("Could not retrieve Alunos", e.getMessage(), e);
                }
                return alunos;
            }

            protected void onPostExecute(CollectionResponseAlunoEqualize alunos) {
                pd.dismiss();
                // Do something with the result.
                ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
                List<AlunoEqualize> _list = alunos.getItems();
                for (AlunoEqualize alunoEqualize : _list) {
                    HashMap<String, String> item = new HashMap<String, String>();
                    item.put("nome", alunoEqualize.getNome());
                    item.put("curso", alunoEqualize.getCurso());
                    list.add(item);
                }
                adapter = new ListAlunoAdapter(getActivity(), list);
                setListAdapter(adapter);
            }
        }

    }
}
